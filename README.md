Homework requirement:

Create a scaffolded-out Android application
Target it to Android IceCreamSandwich
Package should be "edu.washington.<yourNetID>.hello"
Change the "Hello World" text edit to read "Go Seahawks!" or "Go Dawgs!" or "Cougars suck!"
Change the displayed name of the app to "Hello"
Ensure it builds correctly
Ensure it runs inside the emulator
Ensure it runs on an Android device - make sure this device is at least IceCreamSandwich-level capable
Homework grading:

All your code should be in a GitHub repo under your account
repo should be called 'helloandroid'
repo should contain all necessary build artifacts
include a directory called "screenshots", including:
screenshot of app running on emulator
pic or screenshot or movie of app running on a device
We will clone and build it from the GH repo
BONUS: we can run the app from the Google Play Store